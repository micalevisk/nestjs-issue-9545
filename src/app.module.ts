import { APP_INTERCEPTOR } from '@nestjs/core';
import { FactoryProvider, Scope } from '@nestjs/common/interfaces';
import { Module, Injectable, NestInterceptor } from '@nestjs/common';
import { AppController } from './app.controller'

@Injectable()
export class RequestTrackerInterceptor implements NestInterceptor {
  intercept(_, next: any) {
    console.log('intercepted')
    return next.handle()
  }
}

const RequestTrackerFactory: FactoryProvider = {
  provide: APP_INTERCEPTOR,
  scope: Scope.REQUEST,
  useFactory: () => {
    return new RequestTrackerInterceptor()
  }
}

@Module({
  providers: [RequestTrackerFactory],
  exports: [RequestTrackerFactory], // This line is triggering that error
})
class LoggingModule {}

@Module({
  controllers: [AppController],
  imports: [LoggingModule],
})
export class AppModule {}
